# Blue Technical Test
by GM

# Installation instructions

Clone the repo into a folder served by NGINX or Apache, visit the url in the browser and follow instructions for normal WordPress install. File wp-config-sample.php will be used as base of wp-config.php, and it defines the constant EXTERNAL_CATEGORIES_URL, which can be customized.

In WordPress back-end, activate plugins Maintenance (for making the site available only to admins/other users) and then activate the plugin "Taxonomy Maintainer".

To install fake JSON API, run
```
cd external_api; npm install;

```
To run the fake JSON API, run
```
node external_api/node_modules/json-server/bin/index.js --watch external_api/db.json

```

# Notes on the application

Extra features 1 (deployed in a public site): not done.
Stopped users from adding Taxonomy Terms through WordPress back-end screens: done (for all user roles except for Administrators).

All the code is in the plugin "Taxonomy Maintainer", specifically in the all-static class of the same name. The comments on the header and body of the method explain development details.

The main assumption about the requirements was this: id of Taxonomy Terms from the external API is expected to be different from internal ID from WordPress Terms table (as WordPress can manage many independent Taxonomies and all terms through them share the same table and unique ID column, it makes no sense that the external API IDs coincide with WordPress external APIs). So the challenge was to use term-metadata to store the external APIs and still maintain proper relationships and updating. The core of this operation is in the method performUpdate in the main plugin class, all the other methods being only WordPress hooks.

I decided to work in WordPress because of the instructions, although I would have felt more comforatable and efficient in Laravel or even plain PHP. I also didn't use the WordPress $wpdb API, that would have allowed avoiding the n+1 query problem when requesting metadata for the Terms (or updating/inserting), to give preference to WordPress standard API.

# Work left in progress 

Unit tests for the methods getEntersExitsAndUpdates and performUpdate.
Improvements over visualization (noted in TODO: lines in code).
Split the method performUpdate in even smaller, isolated, testable units.
Optimize the algorithms for traversing enter/exits/updates.
Use composer-versioning for WordPress and the plugins (to be able to distribute only the Plugin Code).
Make the maintenance of the Cron schedule in Action Hook 'init' instead of Plugin Activation Hook.

# A short paragraph outlining what you thought of the test

A good test, mixing normal WordPress and inter-operating skills with hidden quirks (like the external ID issue).

# How long the test took to complete

5 or 6 hours, with long pauses for family time.


