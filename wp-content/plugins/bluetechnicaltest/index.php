<?php
/**
 * @TaxonomyMaintainer
 * @version 1.6
 */
/*
Plugin Name: Taxonomy Maintainer
Plugin URI: 
Description: It maintains a WP Taxonomy synchronised to an external API called via AJAX
Author: Gabriel Merida
Version: 1.0
Author URI: http://www.gabrielmerida.cl/
*/

require_once plugin_dir_path( __FILE__ ) . 'TaxonomyMaintainer.php';

// All the code is in the class TaxonomyMaintainer, with its only entry point
TaxonomyMaintainer::runPlugin();

// We must register the activation/deactivation hooks here
register_activation_hook(__FILE__, ['TaxonomyMaintainer', 'hookActivatePlugin']);
register_deactivation_hook(__FILE__, ['TaxonomyMaintainer', 'hookDeactivatePlugin']);

