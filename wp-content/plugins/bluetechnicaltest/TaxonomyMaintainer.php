<?php

class TaxonomyMaintainer {

    /**
     * External entry point to the class. Registers the actions for the plugin
     * @return null
     */
    static function runPlugin()
    {
        // we just need 4 actions
        // for registering the taxonomies
        add_action('init', ['TaxonomyMaintainer', 'hookInit']);
        // a backend page under Plugins submenu
        add_action('admin_menu', ['TaxonomyMaintainer', 'hookAdminMenu']);
        // and an ajax point to call the synchronization from the back-end
        add_action('wp_ajax_updatetaxonomies', ['TaxonomyMaintainer', 'hookAjaxUpdateTaxonomies']);
        // and for the cron call
        add_action('cron_update_taxonomies', ['TaxonomyMaintainer', 'hookCronUpdateTaxonomies']);

        // We need to add a special cron frequency
        add_filter('cron_schedules', ['TaxonomyMaintainer', 'filterAddCron']);

    }
    /**
     * Plugin initialization tasks. Only register the taxonomy
     * @return null
     */
    static function hookInit()
    {
        register_taxonomy(
            'external_category',
            'post', // sensible default
            [
                'label' => 'External Category',
                'hierarchical' => true
            ]
        );
    }

    /**
     * We need to add a wp-cron preset of 30 minutes, as WP doesn't provide one
     * @param  array $schedules WP Schedules
     * @return array            same $schedules, extended
     */
    static function filterAddCron($schedules)
    {
        if (!isset($schedules["30_minutes"])) {
            $schedules["30_minutes"] = [
                'interval' => 30 * 60,
                'display' => __('Once every 1/2 hour')
            ];
        }
        return $schedules;
    }

    /**
     * We schedule the cron call only on plugin activation
     * @return null
     */
    static function hookActivatePlugin()
    {
        if (!wp_next_scheduled('update_categories')) {
            $ret = wp_schedule_event(time(), '30_minutes', 'update_categories');
        }

        // we'll also remove the capability of adding new tags for all
        // pre-existing roles
        global $wp_roles;
        // except administrator
        // $wp_roles->remove_cap('administrator', 'manage_categories'); 
        $wp_roles->remove_cap('editor', 'manage_categories'); 
        $wp_roles->remove_cap('author', 'manage_categories'); 
        $wp_roles->remove_cap('contributor', 'manage_categories'); 
        $wp_roles->remove_cap('subscriber', 'manage_categories'); 
    }  

    /**
     * And we de-schedule on plugin desactivation
     * @return null
     */
    static function hookDeactivatePlugin()
    {
        wp_clear_scheduled_hook('update_categories');
    }    

    /**
     * Called every 30 minutes through WP Cron system
     * It has the same effect than calling the ajax endpoint of the plugin:
     * calls the main function performUpdate();
     * @return null
     */
    static function hookCronUpdateTaxonomies()
    {
        $resultUpdate = self::performUpdate();
        // not doing anything with $result, but we could log it, somewhere
    }
    
    /**
     * Adds an entry to the sidebar menus, under Plugins
     * @return null
     */
    static function hookAdminMenu()
    {
        add_plugins_page('Update Taxonomies', 'Update Taxonomies', 'read', 'update_taxonomies', ['TaxonomyMaintainer', 'viewBackendPage']);
    }

    /**
     * Takes care of answering to the ajax-call originated by the button in the
     * backend page. It calls the main method performUpdate()
     * @return null
     */
    static function hookAjaxUpdateTaxonomies()
    {

        $resultUpdate = self::performUpdate();
        $debug = [
            'resultUpdate' => $resultUpdate
        ];
        header('Content-Type: application/json');
        echo json_encode($debug);
        wp_die();
    }

    /**
     * The content of the back-end page
     * TODO: Improve this button with front-end messages, like
     * "Categories have been updated".
     * @return null
     */
    static function viewBackendPage()
    {
        echo <<<"HTML"
            <h1>Update Taxonomies</h1>
            <p>Just press the button to update your taxonomies now</p>
            <p><button onclick="(function(){xhr = new XMLHttpRequest();xhr.onload = function(){console.log(JSON.parse(xhr.responseText))};xhr.open('POST', ajaxurl);xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');xhr.send('action=updatetaxonomies');})()">Update categories now</button></p>
HTML;
    }

    /**
     * Calls the external API and updates the WP Taxonomy "external_category" accordingly
     * This is the main function of the plugin
     * @return array    Debug or report information 
     */
    static function performUpdate()
    {
        // A helper function, a simplified version of Laravel Collection::keyBy
        $keyBy = function (Callable $accessor, array $array) {
            $results = [];
            foreach ($array as $item) {
                $results[$accessor($item)] = $item;
            }
            return $results;
        };

        $categoriesUrl = EXTERNAL_CATEGORIES_URL;
        $result = wp_remote_get($categoriesUrl);
        if ($result instanceof WP_Error) return false;
        if (!($externalTerms = json_decode($result['body']))) return false;
        
        /**
        we will work with two sources arrays:
        1. $internalTerms, based on WP list of Terms objects (for the relevant Taxonomy)
            and every member extended with properties 
            'parent_external_category_id' and 'external_category_id'
        2. $externalTerms contains all items taken from external API
        */

        // query all our terms
        // Note:
        // this mapping is the problem n+1 in DB queries.
        // It could be avoided if there is a WP Function that queries terms WITH its metadata,
        // or using directly the class WPDB. 
        $internalTerms = array_map(
            function ($term) {
                return array_merge(
                    (array) $term,
                    [
                        'external_category_id' => (int) get_term_meta(
                            $term->term_id,
                            'external_category_id',
                            true
                        ),
                    ]
                );
            },
            get_terms([
                'taxonomy' => 'external_category',
                'hide_empty' => false,
                'meta_query' => [
                    'key' => 'external_category_id'
                ]
            ])
        );

        // two helper arrays
        // TODO: this could be moved to the previous array_map to avoid re-cycling
        $internalTermsKeyedByTermId = $keyBy(function ($d) {
            return $d['term_id'];
        }, $internalTerms);

        // this must be done AFTER a round through $internalTerms
        $internalTerms = array_map(function ($term) use ($internalTermsKeyedByTermId) {
            $foundParentTerm = $term['parent']
                ? $internalTermsKeyedByTermId[$term['parent']]['external_category_id']
                : null;

            return (object) array_merge(
                $term,
                [
                    'parent_external_category_id' => $foundParentTerm
                ]
            );
        }, $internalTerms);

        // let's process these two lists and get the enters, exits and updates,
        // as selections of keys of the original arrays
        list($entersExternalKeys, $exitsInternalKeys, $updatesBothKeys) = self::getEntersExitsAndUpdates(
            $internalTerms,
            $externalTerms
        );

        // let's add to the taxonomy vocabulary all the 'enters', terms in the
        // external API not present in the WP Taxonomy
        array_walk($entersExternalKeys, function($key) use (&$internalTerms, $externalTerms) {
            $newTermReturn = wp_insert_term(
                $externalTerms[$key]->name,
                'external_category'
            );
            if ($newTermReturn instanceof WP_Error) return;
            $newTerm = get_term($newTermReturn['term_id']);
            $newTerm = (object) array_merge(
                (array) $newTerm,
                [
                     'external_category_id' => $externalTerms[$key]->id
                ]
            );
            $internalTerms[] = $newTerm;
            update_term_meta(
                $newTermReturn['term_id'],
                'external_category_id',
                $externalTerms[$key]->id
            );
        });

        // we generate the helper $internalTermsKeyedByExternalTermId now that
        // we have the old $internalTerms added with the newly added $enters 
        $internalTermsKeyedByExternalTermId = $keyBy(function ($d) {
            return $d->external_category_id;
        }, $internalTerms);

        // and then we walk again the array to set the parent_id property of each
        // recently added term
        array_walk($entersExternalKeys, function ($key) use (
            $internalTermsKeyedByExternalTermId, $externalTerms
        ) {
            $externalTerm = $externalTerms[$key];
            wp_update_term(
                $internalTermsKeyedByExternalTermId[$externalTerm->id]->term_id,
                'external_category',
                [
                    'parent' => $internalTermsKeyedByExternalTermId[$externalTerm->parent_id]->term_id,
                ]
            );
        });


        // Let's remove the exits from the WP Taxonomy
        array_walk($exitsInternalKeys, function ($internalKey) use ($internalTerms) {
            wp_delete_term(
                $internalTerms[$internalKey]->term_id,
                'external_category'
            );
        });

        // and let's review the updates, IDs present in both the external API 
        // and the WP Taxonomy, that must be updated if name or parent_id changes
        array_walk($updatesBothKeys, function ($updateBothKeys) use (
            $internalTerms, $externalTerms, $internalTermsKeyedByExternalTermId
        ) {
            $internalTerm = $internalTerms[$updateBothKeys['internalKey']];
            $externalTerm = $externalTerms[$updateBothKeys['externalKey']];
            if (
                $internalTerm->name !== $externalTerm->name ||
                $internalTerm->parent_external_category_id !== $externalTerm->parent_id
            ) {
                wp_update_term(
                    $internalTerm->term_id,
                    'external_category',
                    [
                        'name' => $externalTerm->name,
                        'parent' => $internalTermsKeyedByExternalTermId[$externalTerm->parent_id]->term_id,
                    ]
                );
            }
        });

        // Everything went well and we return
        // TODO: This would be a good place to add logging
        return true;
    }

    /**
     * pure function that compares the two arrays and returns three arrays
     * containing enters (present in second list but not in first),
     * exits (present in first list but not in second)
     * and updates (present in both)
     * The ID used is not the WP Term ID, but the ID of the external API
     * (That in WP Taxonomy is stored as term metadata 'external_category_id')
     * @param  array $internalTerms Copy of WP_Term object with a couple of added properties
     * @param  array $externalTerms Terms from the external API
     * @return [
     *         array $enters,       array of keys for the array $internalTerms
     *         array $exits,        array of keys for the array $externalTerms
     *         array $updates       array of arrays with keys ['internalKey' and 'externalKey']
     * ]
     *            
     */
    static function getEntersExitsAndUpdates($internalTerms, $externalTerms)
    {
        $idArrayComparer = function ($a, $b) {
            return $a['id'] - $b['id'];
        };
        $keyArrayGetter = function ($d) {
            return $d['key'];
        };
        $internalIds = array_map(function ($k, $d) {
            return [
                'key' => $k,
                'id' => $d->external_category_id
            ];
        }, array_keys($internalTerms), $internalTerms);
        $externalIds = array_map(function ($k, $d) {
            return [
                'key' => $k,
                'id' => $d->id
            ];
        }, array_keys($externalTerms), $externalTerms);

        $updatesInternal = array_uintersect($internalIds, $externalIds, $idArrayComparer);
        $updatesExternal = array_uintersect($externalIds, $internalIds, $idArrayComparer);
        usort($updatesInternal, $idArrayComparer);
        usort($updatesExternal, $idArrayComparer);
        $updates = array_map(function ($internal, $external) {
            return [
                'id' => $internal['id'],
                'internalKey' => $internal['key'],
                'externalKey' => $external['key'],
            ];
        }, $updatesInternal, $updatesExternal);

        $enters = array_udiff($externalIds, $updatesInternal, $idArrayComparer);

        $exits = array_udiff($internalIds, $updatesInternal, $idArrayComparer);

        // basically, we have left the intersection/diff task to PHP array functions
        // We are taking many loops, but the growth is lineal

        return [
            array_map($keyArrayGetter, $enters),
            array_map($keyArrayGetter, $exits),
            $updates,
        ];
    }

}
